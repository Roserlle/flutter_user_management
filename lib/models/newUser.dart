import 'package:freezed_annotation/freezed_annotation.dart';

part 'newUser.freezed.dart';
part 'newUser.g.dart';

@freezed
class NewUser with _$NewUser {
	const factory NewUser({
		required String name,
		required String email,
		required String phone,
		required String website,
	}) = _NewUser;

	factory NewUser.fromJson(Map<String, dynamic> data) => _$NewUserFromJson(data);
}