// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'newUser.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_NewUser _$_$_NewUserFromJson(Map<String, dynamic> json) {
  return _$_NewUser(
    name: json['name'] as String,
    email: json['email'] as String,
    phone: json['phone'] as String,
    website: json['website'] as String,
  );
}

Map<String, dynamic> _$_$_NewUserToJson(_$_NewUser instance) =>
    <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
      'phone': instance.phone,
      'website': instance.website,
    };
