// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'newUser.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

NewUser _$NewUserFromJson(Map<String, dynamic> json) {
  return _NewUser.fromJson(json);
}

/// @nodoc
class _$NewUserTearOff {
  const _$NewUserTearOff();

  _NewUser call(
      {required String name,
      required String email,
      required String phone,
      required String website}) {
    return _NewUser(
      name: name,
      email: email,
      phone: phone,
      website: website,
    );
  }

  NewUser fromJson(Map<String, Object> json) {
    return NewUser.fromJson(json);
  }
}

/// @nodoc
const $NewUser = _$NewUserTearOff();

/// @nodoc
mixin _$NewUser {
  String get name => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get phone => throw _privateConstructorUsedError;
  String get website => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $NewUserCopyWith<NewUser> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NewUserCopyWith<$Res> {
  factory $NewUserCopyWith(NewUser value, $Res Function(NewUser) then) =
      _$NewUserCopyWithImpl<$Res>;
  $Res call({String name, String email, String phone, String website});
}

/// @nodoc
class _$NewUserCopyWithImpl<$Res> implements $NewUserCopyWith<$Res> {
  _$NewUserCopyWithImpl(this._value, this._then);

  final NewUser _value;
  // ignore: unused_field
  final $Res Function(NewUser) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? email = freezed,
    Object? phone = freezed,
    Object? website = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      phone: phone == freezed
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      website: website == freezed
          ? _value.website
          : website // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$NewUserCopyWith<$Res> implements $NewUserCopyWith<$Res> {
  factory _$NewUserCopyWith(_NewUser value, $Res Function(_NewUser) then) =
      __$NewUserCopyWithImpl<$Res>;
  @override
  $Res call({String name, String email, String phone, String website});
}

/// @nodoc
class __$NewUserCopyWithImpl<$Res> extends _$NewUserCopyWithImpl<$Res>
    implements _$NewUserCopyWith<$Res> {
  __$NewUserCopyWithImpl(_NewUser _value, $Res Function(_NewUser) _then)
      : super(_value, (v) => _then(v as _NewUser));

  @override
  _NewUser get _value => super._value as _NewUser;

  @override
  $Res call({
    Object? name = freezed,
    Object? email = freezed,
    Object? phone = freezed,
    Object? website = freezed,
  }) {
    return _then(_NewUser(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      phone: phone == freezed
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      website: website == freezed
          ? _value.website
          : website // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_NewUser implements _NewUser {
  const _$_NewUser(
      {required this.name,
      required this.email,
      required this.phone,
      required this.website});

  factory _$_NewUser.fromJson(Map<String, dynamic> json) =>
      _$_$_NewUserFromJson(json);

  @override
  final String name;
  @override
  final String email;
  @override
  final String phone;
  @override
  final String website;

  @override
  String toString() {
    return 'NewUser(name: $name, email: $email, phone: $phone, website: $website)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NewUser &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.phone, phone) ||
                const DeepCollectionEquality().equals(other.phone, phone)) &&
            (identical(other.website, website) ||
                const DeepCollectionEquality().equals(other.website, website)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(phone) ^
      const DeepCollectionEquality().hash(website);

  @JsonKey(ignore: true)
  @override
  _$NewUserCopyWith<_NewUser> get copyWith =>
      __$NewUserCopyWithImpl<_NewUser>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_NewUserToJson(this);
  }
}

abstract class _NewUser implements NewUser {
  const factory _NewUser(
      {required String name,
      required String email,
      required String phone,
      required String website}) = _$_NewUser;

  factory _NewUser.fromJson(Map<String, dynamic> json) = _$_NewUser.fromJson;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  String get email => throw _privateConstructorUsedError;
  @override
  String get phone => throw _privateConstructorUsedError;
  @override
  String get website => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$NewUserCopyWith<_NewUser> get copyWith =>
      throw _privateConstructorUsedError;
}
