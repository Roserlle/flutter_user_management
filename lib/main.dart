import 'package:flutter/material.dart';

import '/screens/user_list_screen.dart';
import '/screens/add_user_screen.dart';

void main() {
    runApp(App());
}

class App extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            theme: ThemeData(primaryColor: Color.fromRGBO(20, 45, 68, 1)),
            initialRoute: '/',
            routes: {
                '/': (context) => UserListScreen(),
                '/add_user': (context) => AddUserScreen(),
            }
        );
    }
}