import 'package:flutter/material.dart';

import '/utils/function.dart';

class AppDrawer extends StatelessWidget {
	@override
	Widget build(BuildContext context){
		return Drawer(
			child: Container(
				width: double.infinity,
                color: Color.fromRGBO(20, 45, 68, 1),
				child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                        ListTile(
                            title: addUserButton,
                            onTap: () async {
                               Navigator.pushNamed(context, '/add_user');
                            }
                        )
                    ]
                )
			)
		);
	}
}