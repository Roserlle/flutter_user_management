import 'package:dio/dio.dart';

import '/models/newUser.dart';
import '/models/user.dart';

class API {
    String url = 'https://jsonplaceholder.typicode.com/users';

	Future getUsers() async {
		try {
			var response = await Dio().get(url);
            
			List<User> users = [];

			for (var user in response.data) {
				// The 'user' here is of Map<String, dynamic> type.
				// The 'user' map is converted into a User object.
				// After conversion, the object is added to the list.
				users.add(User.fromJson(user));
			}

			return users;
		} catch (exception) {
			throw exception;
		}
	}

    Future<NewUser> setUser (NewUser user) async {
        NewUser? addedUser;

        try {
            var response = await Dio().post(
                url,
                options: Options(
                    headers: {'Content-Type': 'application/json; charset=UTF-8'}
                ),
                data: user.toJson()
            );

            print(response.data);
            addedUser = NewUser.fromJson(response.data);

            if (response.statusCode == 200) {
                return addedUser;
            } else {
                throw Exception('Failed to add user');
            }
        } catch(exception) {
            throw exception;
        }
    }

    Future<NewUser> updateUser (NewUser user, int id) async {
        NewUser? updatedUser;

        try {
            var response = await Dio().put(
                url + '/$id',
                data: user.toJson()
            );
            
            print(response.data);
            updatedUser = NewUser.fromJson(response.data);

            if (response.statusCode == 200) {
                return updatedUser;
            } else {
                throw Exception('Failed to update user information');
            }
        } catch(exception) {
            throw exception;
        }
    }

}