import 'package:flutter/material.dart';

var addUserButton = RichText(text: TextSpan(
    style: TextStyle(fontSize: 16.0, color: Colors.white),
    children: [
        WidgetSpan(child: Icon(Icons.add_circle_outline, color: Colors.white)),
        TextSpan(text: ' Add User'),
    ]
));

var elevatedButtonStyle = ElevatedButton.styleFrom(
    primary: Colors.orange.shade600,
    onPrimary: Colors.white,
    shadowColor: Colors.black,
    elevation: 5,
);

void showSnackBar(BuildContext context, String message) {
    SnackBar snackBar = new SnackBar(
        content: Text(message), 
        duration: Duration(milliseconds: 2000)
    );
    
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
}