import 'package:flutter/material.dart';

import '/utils/api.dart';
import '/models/user.dart';
import '/widgets/app_drawer.dart';
import '/screens/edit_user_screen.dart';

class UserListScreen extends StatefulWidget {
	@override
	UserListScreenState createState() => UserListScreenState();
}

class UserListScreenState extends State<UserListScreen> {
	Future? futureUsers;

	final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

	List<Widget> generateListTiles(List<User> users) {
		List<Widget> listTiles = [];

		for (User user in users) {
			listTiles.add(ListTile(
				title: Text(user.name, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
				subtitle: Text(user.email),
                trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        IconButton(
                            color: Colors.black,
                            icon: Icon(Icons.edit),
                            onPressed: () async {
                                showDialog(
                                   context: context, 
                                   builder: (BuildContext context) => EditUserScreen(user)
                                );
                            }
                        )
                    ],
                )
			));
		}

		return listTiles;
	}

	@override
	void initState() {
		super.initState();

		WidgetsBinding.instance!.addPostFrameCallback((timestamp) {
			setState(() {
				futureUsers = API().getUsers();
            
			});
		});
	}

	@override
	Widget build(BuildContext context) {

		Widget fbUserList = FutureBuilder(
			future: futureUsers,
			builder: (context, snapshot) {
				if (snapshot.connectionState == ConnectionState.done) {
					if (snapshot.hasError) {
						return Center(
							child: Text('Could not load the user list, restart the app.')
						);
					}

					return RefreshIndicator(
						key: refreshIndicatorKey,
						onRefresh: () async {
							setState(() {
								futureUsers = API().getUsers();
							});
						},
						child: ListView(
							padding: EdgeInsets.all(8.0),
							children: generateListTiles(snapshot.data as List<User>)
						)
					);
				}

				return Center(
					child: CircularProgressIndicator()
				);
			}
		);

		return Scaffold(
			appBar: AppBar(title: Text('User List')),
			endDrawer: AppDrawer(),
			body: Container(
				child: fbUserList
			)
		);
	}
}