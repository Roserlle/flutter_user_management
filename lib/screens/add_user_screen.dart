import 'dart:async';
import 'package:flutter/material.dart';

import '/models/newUser.dart';
import '/utils/api.dart';
import '/utils/function.dart';

class AddUserScreen extends StatefulWidget {
	@override
	AddUserScreenState createState() => AddUserScreenState();
}

class AddUserScreenState extends State<AddUserScreen>{
    Future? _futureAddUsers;

    final _formKey = GlobalKey<FormState>();
    final _txtNameController = TextEditingController();
    final _txtEmailController = TextEditingController();
    final _txtPhoneController = TextEditingController();
    final _txtWebsiteController = TextEditingController();

    void _addUser(BuildContext context) {
        NewUser user = NewUser(
            name: _txtNameController.text,
            email: _txtEmailController.text,
            phone: _txtPhoneController.text,
            website: _txtWebsiteController.text
        );

        setState(() {
            _futureAddUsers = API().setUser(user).catchError((error) {
                showSnackBar(context, error.message);
            });
        });
    }

    

	@override
	Widget build(BuildContext context){
        final FocusScopeNode focusNode = FocusScope.of(context);

        Widget txtName = TextFormField(
            decoration: InputDecoration(labelText: 'Name'),
            keyboardType: TextInputType.text,
            controller: _txtNameController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Name is required.';
            }
        );

        Widget txtEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.text,
            controller: _txtEmailController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid email.';
            }
        );

        Widget txtWebsite = TextFormField(
            decoration: InputDecoration(labelText: 'Website'),
            keyboardType: TextInputType.text,
            controller: _txtWebsiteController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Website is required.';
            }
        );

        Widget txtPhone = TextFormField(
            decoration: InputDecoration(labelText: 'Phone Number'),
            keyboardType: TextInputType.text,
            controller: _txtPhoneController,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Phone Number is required.';
            }
        );

        Widget btnSubmit = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 12.0),
            child: ElevatedButton(
                style: elevatedButtonStyle,
                onPressed: () { 
                    if (_formKey.currentState!.validate()) {
                        _addUser(context);
                        Navigator.pushNamed(context, '/');
                        showSnackBar(context, 'Success');
                    } else {
                        showSnackBar(context, 'Form validation failed. Check input and try again.');
                    }
                }, 
                child: Text('Submit')
            )
        );
        

        Widget formLogin = Form(
            key: _formKey,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    txtName,
                    txtEmail,
                    txtPhone,
                    txtWebsite,
                    btnSubmit
                ]
            ), 
        );

        Widget registerView = FutureBuilder(
            future: _futureAddUsers,
            builder: (context, snapshot){
                if (_futureAddUsers == null){
                    return formLogin;
                } else if (snapshot.hasError == true){
                    return formLogin;
                } else if (snapshot.hasData == true){
                    print(snapshot.data);
                    return Container();
                }
                return Center(
                    child: CircularProgressIndicator()
                );
            }
        );

		return Scaffold(
            appBar: AppBar(
                backgroundColor: Color.fromRGBO(20, 45, 68, 1),
                title: Text('Add User'),
                elevation: 0.0
            ),
            body: SingleChildScrollView(
                child: Padding(
                    padding: EdgeInsets.fromLTRB(16, 25, 16, 8),
                    child: registerView
                )
            )
        );
	}
}